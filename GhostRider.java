package fatec2018;
import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import static robocode.util.Utils.normalRelativeAngle;
import java.awt.*;
import robocode.util.*;
import java.awt.Color;
import robocode.AdvancedRobot; 


public class GhostRider extends RateControlRobot{
	boolean movingForward; // True quando setAhead é chamada e vice-versa
	boolean inWall; // True quando o robô está perto da parede.
	double roboX, roboY; // Variaveis para receber eixo X e eixo Y

	public void run() {
	
		roboX = getX(); // Recebendo a posição de X
		roboY = getY(); // Recebendo a posição de Y	

		// setColors(Color.red,Color.blue,Color.green); // corpo, radar, arma, scanner, bala
		setBodyColor(Color.black); // Cor para o corpo do robô
		setRadarColor(Color.red); // Cor para o radar do robô
		setGunColor(Color.white); // Cor para a arma do robô
		setBulletColor(Color.yellow); // Cor para o tiro do robô
		setScanColor(Color.blue); // Cor para o scanner do robô

		// Cada parte do robô move-se livremente dos outros.
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

		setAhead(1000); // Andar 1000 px a frente
		setTurnRadarRight(360); // Scanner girar 360º


		while (true) {		
			setVelocityRate(3);	// Setando velocidade para 3
	
			if (getX() > 50 && getY() > 50
					&& getBattleFieldWidth() - getX() > 50
					&& getBattleFieldHeight() - getY() > 50
					&& this.inWall == true) {
				this.inWall = false;
			}
			if (getX() <= 50 || getY() <= 50
					|| getBattleFieldWidth() - getX() <= 50
					|| getBattleFieldHeight() - getY() <= 50) {
				if (this.inWall == false) {
					reverseDirection();
					inWall = true;
				}
			}

			// Se o radar parou de girar, procure um inimigo
			if (getRadarTurnRemaining() == 0.0) {
				setTurnRadarRight(360); // Girar radar 360º
			}

			execute(); // executar todas as ações set.
		}
	}

	public void onScannedRobot(ScannedRobotEvent inimigo) {	
	
		double absoluteBearing = getHeading() + inimigo.getBearing(); // Calcular posição exata	
		
		String nomeRobo = inimigo.getName();// Captura o nome dos robos escaneados
		
		if( nomeRobo.equals("fatec2018.ThePunisher*") || nomeRobo.equals("fatec2018.DrStranger*") || nomeRobo.equals("samplesentry.BorderGuard") ){
			scan(); // Escanear novamente
		}else{
			if(inimigo.getDistance() < 150){
				double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading()); // Pegando posição absoluta da arma
				double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing - getRadarHeading()); // Pegando posição absoluta da arma
				fire(2); // Atirar bala número 2
			}else{
				double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading()); // Pegando posição absoluta da arma
				double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing - getRadarHeading()); // Pegando posição absoluta da arma
				fire(1); // Atirar bala número 1
			}
		}
	
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
		setTurnRight(90); // Girar corpo 90º
		setAhead(20); // Andar para frente 20px
	}
		
	public void onHitRobot(HitRobotEvent inimigo){
		setTurnRight(inimigo.getBearing() + getHeading() - getGunHeading());
		fire(3); // Aitrar bala número 3
		setVelocityRate(getVelocity() + 3); // Setando velocidade do robô de atual = atual + 3  
		reverseDirection(); // Reverter a direção
	}

	public void onHitWall(HitWallEvent e) {
		reverseDirection(); // Reverter a diração
	}
	
	// Inverter o sentido do robô - Usado na função onHitWall
	public void reverseDirection() {
		if (this.movingForward) {
			setBack(1000);
			this.movingForward = false;
		} else {
			setAhead(1000);
			this.movingForward = true;
		}
	}
}