package fatec2018;
import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import static robocode.util.Utils.normalRelativeAngle;
import java.awt.*;
import robocode.util.*;
import java.awt.Color;
import robocode.AdvancedRobot;    




public class DrStranger extends RateControlRobot{
		
	boolean movingForward; // True quando quando setAhead é chamada
	double roboX, roboY; // Definindo as váriavéis de posição

	public void run() {
	
		roboX = getX(); // Recebendo a posição de X
		roboY = getY(); // Recebendo a posição de Y

		// Cada parte do robô move-se livremente dos outros.
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);	

		// setColors(Color.red,Color.blue,Color.green); // corpo, radar, arma, scanner, bala
		setBodyColor(Color.black); // cor para o corpo do robô
		setRadarColor(Color.red); // cor para o radar do robô
		setGunColor(Color.white); // cor para a arma do robô
		setBulletColor(Color.yellow); // cor para o tiro do robô
		setScanColor(Color.blue); // cor para o scanner do robô
		
		setTurnRadarRight(360); // Girar radar 360º
		setTurnGunRight(360); // Girar arma 360º
		setTurnRight(90); // Girar corpo 360º

		// Robot main loop
		while(true) {
			 setVelocityRate(5); // Setando velocidade do robô para 5
			 setTurnRateRadians(0); // Determina rotação do robô no sentido horário
			 execute(); // Executar as ações
			 setTurnRadarRight(360); // Girar radar 360°º para direita 
 
		}
	}

	public void onScannedRobot(ScannedRobotEvent inimigo) {
			
			String nomeRobo = inimigo.getName();// Variável que irá armazenar o nome do rôbo escaneado
           
            if (nomeRobo.equals("fatec2018.GhostRider*") || nomeRobo.equals("fatec2018.ThePunisher*") || nomeRobo.equals("samplesentry.BorderGuard")){ 
                scan();
            } else {
                if (inimigo.getDistance() < 150) {
					setTurnGunRight(getHeading() - getGunHeading() + inimigo.getBearing()); // Recebendo dados do robô
                    setFire(2); // Atirar bala número 2
                }else{
					setTurnGunRight(getHeading() - getGunHeading() + inimigo.getBearing()); // Recebendo dados do robô
                    setFire(1); // Atirar bala número 3   
                }
            }
			

			execute();
	
	}

	public void onHitByBullet(HitByBulletEvent e) {
		setTurnRight(90); // Girar corpo 90º para direita
		setAhead(20); // Andar para frente 20px
	}
	

	public void onHitWall(HitWallEvent e) {
		reverseDirection(); // Reverter direção
	}
	
	public void onHitRobot(HitRobotEvent inimigo){
			setTurnRight(inimigo.getBearing() + getHeading() - getGunHeading()); // Recebendo dados do robô
			fire(3); // Atirar bala número 3
			setVelocityRate(getVelocity() + 3); // Setando velocidade do robo para velocidadel(5) + 3 
	}
	
	public void onWin(WinEvent e){
		int variavel = 1;
		while(true){
			setTurnRight(180 * variavel);
			variavel = variavel * (-1);
		}
	}
	
	// Inverter o sentido do robô - Usado na função onHitWall
	public void reverseDirection() {
		if (this.movingForward) {
			setBack(40000);
			this.movingForward = false;
		} else {
			setAhead(40000);
			this.movingForward = true;
		}
	}
	

}
