package TreeRobot;
import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import static robocode.util.Utils.normalRelativeAngle;
import java.awt.*;
import robocode.util.*;
import java.awt.Color;
import robocode.AdvancedRobot; 


public class ThePunisher extends AdvancedRobot{

	boolean movingForward; // True quando setAhead é chamada
	double distancia, coord; // Váriaveis usada para configurar a mira

	public void run() {

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		setBodyColor(Color.black); // Cor para o corpo do robô
		setRadarColor(Color.red); // Cor para o radar do robô
		setGunColor(Color.white); // Cor para a arma do robô
		setBulletColor(Color.yellow); // Cor para o tiro do robô
		setScanColor(Color.blue); // Cor para o scanner do robô

		while(true) {
			setAhead(100); // Andar para frente 100px
			setTurnGunRight(360); // Girar arma 360º
			setBack(100); // Andar para trás 100px
			setTurnRadarRight(360); // Girar radar 360º
			execute();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
	
		double absoluteBearing = getHeading() + e.getBearing();	// Calcular a posição exata do robô


		double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading()); // Pegando posição exata da arma
		double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing - getRadarHeading()); // Pegando posição exata do radar

		
		if (this.movingForward) {
			setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 80)); // Girando o corpo para angulo do adversário + 80º
		} else {
			setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 100)); // Girando o corpo para angulo do adversário + 100º
		}

	
		if (Math.abs(bearingFromGun) <= 4) {
			setTurnGunRight(bearingFromGun); // Canhão fixado sobre o inimigo
			setTurnRadarRight(bearingFromRadar); // Radar fixado sobre o inimigo
	
			// Mais próximo, maior o tiro
			if (getGunHeat() == 0 && getEnergy() > .2) {
				fire(Math.min(
						4.5 - Math.abs(bearingFromGun) / 2 - e.getDistance() / 250, 
						getEnergy() - .1));
			}
		} 
		else {
			setTurnGunRight(bearingFromGun);
			setTurnRadarRight(bearingFromRadar);
		}

		// Scanear robô
		if (bearingFromGun == 0) {
			scan();
		}
	}
	
	// Método de quando o robô bate em outro
	public void onHitRobot(HitRobotEvent inimigo){
		setTurnRight(inimigo.getBearing() + getHeading() - getGunHeading()); // Girando corpo conforme os dados recebido do inimigo
		fire(3); // Atirar bala número 3
	}
	
	// Método de quando o robô é atingido por uma bala
	public void onHitByBullet(HitByBulletEvent e) {
		setBack(100); // Voltar 100px
		setTurnRight(180); // Girar corpo 180º
		setAhead(100); // Andar para frente 100px
		setTurnLeft(180); // Girar corpo 180º
	}

	// Método de quando o robô bate na parede
	public void onHitWall(HitWallEvent e) {
		setTurnRight(180); // Girar 180º
		setAhead(100); // Andar para frente 100px
		reverseDirection(); // Reverter direção
	}
	
	// Método dança da vitória
	public void onWin(WinEvent e){
		setTurnRight(360); // Girar corpo 360º
		setAhead(10); // Andar 10 px
		setTurnRadarRight(360); // Girar radar 360º para direita
		setFire(1); // Atirar fogo número 1
	}	

	// Método de reverter diração
	public void reverseDirection() {
		if (this.movingForward) {
			setBack(40000);
			this.movingForward = false;
		} else {
			setAhead(40000);
			this.movingForward = true;
		}
	}
	
	// Configuração da mira
	public void mira(double posIni, double energIni, double energiaRobo){
		distancia = posIni;
		coord = getHeading() + posIni - getGunHeading(); // Obtendo a posição do robô
		setTurnGunRight(coord); // Girar arma para posição do inimigo
		
		if (distancia < 50){
			fire(3); // Atirar fogo número 3
		}else if (distancia > 50 && distancia < 100) // Distância entra 50 - 100
			fire(2); // Atirar fogo número 2
			else 
			fire(1); // Atirar fogo número 1
	}
	
	
}
